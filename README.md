# IDS 721 Individual Project 2
This project involves developing a simple REST API microservice in Rust. It includes a Dockerfile for containerization, ensuring consistent deployment environments, and incorporates CI/CD pipeline configurations.

## Note
The demo video is in the gitlab folder named as `dl402_Individual2.mov`

## Prerequisites

Before you start, ensure you have the following installed:
- Rust and Cargo
- Docker

## Build Process:
Follow these steps to set up and run your project:

1. **Create a New Rust Project:**
   Initialize a new Rust project using Cargo.
   
   ```
   cargo new <PROJECT_NAME>
   ```
2. **Add Dependencies:** 
    Add the required dependencies to your Cargo.toml file by running 

    ```
    actix-web = "4"
    rand = "0.8"
    serde = { version = "1.0", features = ["derive"] }
    ```
3. **Add Functionalities:**
    Add the code for ingesting data and query data in the main.rs file. The main functionality for this project is to create a HTML template that allows user to take a guess between number from 1 to 10. There will be a randomly generated number each time the users take a guess and it will respond with either the answer is correct or wrong.

4. **Run App Locally:**
    Use `cargo run` to run and test whether the app works locally. You can go to `localhost:8080` to see whether the desired page shows up and test the functionality of the app.

5. **Write Dockerfile to containarize the app:**
    Make a new file called `Dockerfile` and add the steps to buld an image.
    Run the following command to build the image for the app: 
    ```
    docker build -t <YOUR IMAGE NAME> .
    ```
    Run the following command to run the containarized application:
    ```
    docker run -p 8080:8080 <YOUR IMAGE NAME>
    ```

6. **Add .gitlab-ci.yml file for pipeline:**
    This file will automatically run whenever codes/files is pushed to gitlab for CI/CD purpose.
    ```
    image: docker:25.0.3

    variables:
    IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""

    services:
    - docker:dind

    stages:
    - test

    test:
    stage: test
    before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    script:
        - docker build -t guess_number_service .
        - docker run -d -p 8080:8080 guess_number_service
        - docker ps -a
    ```

## Screenshots
- Screenshot of Docker running: 
![Alt text](/screenshots/Docker.png)

- Screenshot of the webpages: 
![Alt text](/screenshots/Main.png)
![Alt text](/screenshots/Correct.png)
![Alt text](/screenshots/Wrong.png)