use actix_web::{web, App, HttpResponse, HttpServer, Responder, post};
use rand::Rng;
use serde::Deserialize;

#[derive(Deserialize)]
struct Guess {
    guess: u32,
}

async fn index() -> impl Responder {
    let html = r#"
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Guess the Numbe</title>
        </head>
        <body>
            <h1>Guess the Number!</h1>
            <form action="/guess" method="post">
                <label for="guess">Your guess:</label>
                <input type="text" id="guess" name="guess">
                <input type="submit" value="Guess">
            </form>
        </body>
        </html>
    "#;

    HttpResponse::Ok().content_type("text/html").body(html)
}

#[post("/guess")]
async fn guess_number(form: web::Form<Guess>) -> impl Responder {
    let secret_number = rand::thread_rng().gen_range(1..=10);
    if form.guess == secret_number {
        HttpResponse::Ok().body("Congratulations! You guessed the number!")
    } else {
        HttpResponse::Ok().body(format!("Sorry, the number was: {}", secret_number))
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(index))
            .service(guess_number)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}